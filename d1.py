data = [int(val.strip()) for val in open("d1.txt")]
def filter(data, shift=1):
    return sum([data[i] > data[i-shift] for i in range(shift, len(data), 1)])

print(filter(data))
print(filter(data, 3))