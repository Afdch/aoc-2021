from itertools import pairwise

d = {}
cubes = []
anchors = {n: set() for n in 'XYZ'}
with open('d22.txt') as f:
    for line in f:
        instr, coords = line.strip().split()
        cube = [list(map(int, x[2:].split('..'))) for x in coords.split(',')]
        X, Y, Z = cube
        x1, x2 = X
        y1, y2 = Y
        z1, z2 = Z
        
        instr = True if instr == 'on' else False
        cubes.append((cube, instr))
        
        anchors['X'].update(X)
        anchors['Y'].update(Y)
        anchors['Z'].update(Z)

for axis in anchors:
    anchors[axis] = list(sorted(list(anchors[axis])))

field = {(x, y, z): False for x in anchors['X'] for y in anchors['Y'] for z in anchors['Z']}


size = {}
for xi, X in enumerate(anchors['X']):
    if xi < len(anchors['X']) - 1:
        dx = anchors['X'][xi + 1] - anchors['X'][xi]
    else:
        dx = 1
        
    for yi, Y in enumerate(anchors['Y']):
        if yi < len(anchors['Y']) - 1:
            dy = anchors['Y'][yi + 1] - anchors['Y'][yi]
        else:
            dy = 1
            
        for zi, Z in enumerate(anchors['Z']):
            if zi < len(anchors['Z']) - 1:
                dz = anchors['Z'][zi + 1] - anchors['Z'][zi]
            else:
                dz = 1
                
            size[(X, Y, Z)] = dx * dy * dz

# print(anchors)
for c, instr in cubes:
    x1, x2 = c[0]
    for x in anchors['X']:
        if x1 <= x <= x2:
            y1, y2 = c[1]
            for y in anchors['Y']:
                if y1 <= y <= y2:
                    z1, z2 = c[2]
                    for z in anchors['Z']:
                        if z1 <= z <= z2:
                            field[(x, y, z)] = instr

s = 0
for chunk in size:
    if field[chunk]:
        s += size[chunk]

print(s)