import networkx as nx
from networkx.algorithms.shortest_paths.generic import shortest_path
from printd import printd

map = {}
max_x, max_y = 0, 0
with open('d15.txt') as f:
    for ln, line in enumerate(f):
        for nn, n in enumerate(line.strip()):
            map[(nn, ln)] = int(n)
            if nn > max_x:
                max_x = nn
        if ln > max_y:
            max_y = ln

def neighbours(c, max_x, max_y):
    cy, cx = c
    n = []
    if cy > 0:
        n.append((cy-1, cx))
    if cx > 0:
        n.append((cy, cx-1))
    if cy < max_y:
        n.append((cy+1, cx))
    if cx < max_x:
        n.append((cy, cx+1))
    return n

# part 2:
for i in range(5):
    for j in range(5):
        if (i, j) == (0, 0):
            continue
        for x in range(max_x+1):
            for y in range(max_y+1):
                new_risk = map[(x, y)] + i + j
                if new_risk > 9:
                    new_risk = (new_risk) % 10 + 1
                map[(x + (1+max_x)*(i), y + (1+max_y)*(j))] = new_risk
max_x = (max_x+1)*5-1
max_y = (max_y+1)*5-1

D = nx.DiGraph()
for c in map:
    for n in neighbours(c, max_x, max_y):
        D.add_edge(c, n, weight=map[n])
        
# to print a map
# printd(map, {x:str(x) for x in range(10)}, reverse=False)
        
print(sum(map[x] for x in shortest_path(D, (0,0), (max_x, max_y), 'weight')) - map[(0,0)])