from typing import Counter


with open('d14.txt') as f:
    lines = f.readlines()
    
template = lines[0].strip()

rules = {a: b for a, b in [l.strip().split(' -> ') for l in lines [2:]]}

for step in range(40):
    insertions = []
    for i, e in enumerate(template):
        if i > 0:
            p = template[i-1] + template[i]
            if p in rules:
                insertions.append((i, rules[p]))
    for i, (j, p) in enumerate(insertions):
        template = template[:i+j] + p + template [i+j:]
    #print("".join(template))
    print(step)

C = Counter(template).values()
print(max(C)-min(C))
    