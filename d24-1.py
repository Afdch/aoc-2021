def a1(inp):
    w = inp.pop(0)
    z = w + 13
    #print(z)
    
    w = inp.pop(0)
    z = z * 26 + w + 16
    #print(z)
    
    w = inp.pop(0)
    z = z * 26 + w + 2
    #print(z)
    
    w = inp.pop(0)
    z = z * 26 + w + 8
    #print(z)
    
    w = inp.pop(0)
    z = z * 26 + w + 11
    #print(z)
    
    
    w = inp.pop(0)
    x = 0 if z % 26 - 11 == w else 1
    y = 25 * x + 1
    z = z // 26 * y + (w + 6) * x
    #print(z)
    
    
    w = inp.pop(0)
    z = z * 26 + 12 + w
    #print(z)
    
    
    w = inp.pop(0)
    x = 0 if z % 26 - 16 == w else 1
    y = 25 * x + 1
    z = z // 26 * y + (w + 2) * x
    #print(z)
    
    w = inp.pop(0)
    x = 0 if z % 26 - 9 == w else 1
    y = 25 * x + 1
    z = z // 26 * y + (w + 2) * x
    #print(z)

    w = inp.pop(0)
    z = z * 26 + w + 15
    #print(z)

    w = inp.pop(0)
    x = 0 if z % 26 - 8 == w else 1
    y = 25 * x + 1
    z = z // 26 * y + (w + 1) * x
    #print(z)

    w = inp.pop(0)
    x = 0 if z % 26 - 8 == w else 1
    y = 25 * x + 1
    z = z // 26 * y + (w + 10) * x
    #print(z)

    w = inp.pop(0)
    x = 1 if w != (z % 26 - 10) else 0
    z = z // 26
    z = z * (25 * x + 1)
    z = z + (w + 14) * x
    #print(z)

    w = inp.pop(0)
    x = 0 if (z % 26 - 9) == w else 1
    y = 25 * x + 1
    z = z // 26 * y + (w + 10) * x
    #print(z)
    
    return z

