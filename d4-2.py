with open('d4.txt') as f:
    all_file = ''
    for line in f:
        all_file += line

all_file = [x for x in all_file.split('\n\n')]
numbers = [int(x) for x in all_file[0].split(',')]
boards = {i:[int(x) for x in line.replace('\n', ' ').split()] for i, line in enumerate(all_file[1:])}
win_combs = {i: [set(boards[i][b::5]) for b in range(5)] + [set(boards[i][5*b:5*b+5]) for b in range(5)] for i in boards.keys()}

active_numbers = set()
boards_won = set()
for number in numbers:
    active_numbers.add(number)
    for board in win_combs:
        if board in boards_won:
            continue
        for line in win_combs[board]:
            if line <= active_numbers:
                boards_won.add(board)
                if len(boards_won) == 1:
                    print(f'Part 1 board {board} with a score of {sum(set(boards[board]) - active_numbers)*number}')
                elif len(boards_won) == len(boards):
                    print(f'Part 2 board {board} with a score of {sum(set(boards[board]) - active_numbers)*number}')
                break