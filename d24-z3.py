from z3 import *
s= Optimize()

W = [Int(f'w_{i}') for i in range(14)]
Z = [Int(f'z_{i}') for i in range(14)]


# inp w0
# z = w + 13
s.add(Z[1] == W[0] + 13)


# inp w1
# z = z * 26 + w + 16
s.add(Z[2] == Z[1] * 26 +  W[1] + 16)


# inp w2
# z = z * 26 + w + 2
s.add(Z[3] == Z[2] * 26 +  W[2] + 2)


# inp w3
# z = z * 26 + w + 8
s.add(Z[4] == Z[3] * 26 +  W[3] + 8)


# inp w4
# z = z * 26 + w + 11
s.add(Z[5] == Z[4] * 26 +  W[4] + 11)


# inp w5
# x = 0 if z % 26 - 11 == w else 1
# y = 25 * x + 1
# z = z // 26 * y + (w + 6) * x
s.add(W[5] + 11 == Z[5] % 26)
s.add(Z[6] == Z[5] / 26)


# inp w6
# z = z * 26 + 12 + w
s.add(Z[7] == Z[6] * 26 +  W[6] + 12)


# inp w7
# x = 0 if z % 26 - 16 == w else 1
# y = 25 * x + 1
# z = z // 26 * y + (w + 2) * x
s.add(W[7] + 16 == Z[7] % 26)
s.add(Z[8] == Z[7] / 26)


# inp w8
# x = 0 if z % 26 - 9 == w else 1
# y = 25 * x + 1
# z = z // 26 * y + (w + 2) * x
s.add(W[8] + 9 == Z[8] % 26)
s.add(Z[9] == Z[8] / 26)

# inp w9
# z = z * 26 + w + 15
s.add(Z[10] == Z[9] * 26 +  W[9] + 15)


# inp w10
# x = 0 if z % 26 - 8 == w else 1
# y = 25 * x + 1
# z = z // 26 * y + (w + 1) * x
s.add(W[10] + 8 == Z[10] % 26)
s.add(Z[11] == Z[10] / 26)


# inp w11
# x = 0 if z % 26 - 8 == w else 1
# y = 25 * x + 1
# z = z // 26 * y + (w + 10) * x
s.add(W[11] + 8 == Z[11] % 26)
s.add(Z[12] == Z[11] / 26)


# inp w12
# x = 1 if w != () z % 26 - 10) else 0
# z = z // 26
# z = z * (25 * x + 1)
# z = z + (w + 14) * x
s.add(W[12] + 10 == Z[12] % 26)
s.add(Z[13] == Z[12] / 26 )


# inp w13
# x = 1 if w != (z % 26 - 9) else 0
# z = z * (25 * x + 1) + x * (w + 10)
s.add(W[13] + 9 == Z[13] % 26)
s.add(0 == Z[13] / 26)

# limits for input digits

[s.add(W[n] > 0) for n in range(14)]
[s.add(W[n] < 10) for n in range(14)]


# min/max each value starting from the highest

for n in range(14):
    #m = s.maximize(W[n])
    m = s.minimize(W[n])
    s.check()
    s.add(W[n] == m.value())

s.set('priority', 'box')
is_sat = s.check()
if is_sat:
    print(s.model())