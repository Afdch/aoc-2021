import networkx as nx

G = nx.DiGraph()

Gr = {0: {}, 1: {}}
start = ((4, 8), (0, 0), 0)
game_states = [start]
processed = []

while True:    
    if len(game_states) == 0:
        break
    gs = game_states.pop()
    if gs in processed:
        continue
    processed.append(gs)
    
    positions, scores, player = gs
    
    for dice_result in [3, 4, 5, 5, 6, 6, 7, 7, 8, 9]:
        new_position = ( positions[player] + dice_result - 1 ) % 10 + 1
        new_score = scores[player] + new_position
        if new_score >= 21:
            other_node = player
        else:
            new_positions = list(positions)
            new_scores = list(scores)
            new_positions[player] = new_position
            new_scores[player] = new_score
            state = (tuple(new_positions), tuple(new_scores), (player + 1) % 2)
            game_states.append(state)
            other_node = state
        if other_node not in Gr:
            Gr[other_node] = {}
        
        if gs in Gr[other_node]:
            Gr[other_node][gs] += 1
        else:
            Gr[other_node][gs] = 1

def reduce(s):
    parents = Gr[s]
    su = 0
    for parent in parents:
        if parent == start:
            return Gr[s][parent]
        else:
            su += Gr[s][parent] * reduce(parent)
    return su
        
        
        
print(reduce(0))
print(reduce(1))
