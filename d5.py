pipes = {}

def add_pipe(c):
    global pipes
    if c not in pipes:
        pipes[c] = 0
    pipes[c] += 1
    
with open('d5.txt') as f:
    for line in f:
        coords = line.strip().split(' -> ')
        x1, y1 = coords[0].split(',')
        x1, y1 = int(x1), int(y1)
        x2, y2 = coords[1].split(',')
        x2, y2 = int(x2), int(y2)

        
        
        if x1 == x2:
            #print(f"from {x1},{y1} to {x2},{y2}")
            y = sorted([y1, y2])
            for y in range(y[0], y[1]+1):
                add_pipe((x1, y))      
        
        elif y1 == y2:            
            #print(f"from {x1},{y1} to {x2},{y2}")
            x = sorted([x1, x2])
            for x in range(x[0], x[1]+1):
                add_pipe((x, y1))
        
        # part2 only
        else:
            #diagonals            
            #print(f"from {x1},{y1} to {x2},{y2}")
            kx = -1 if x1 > x2 else 1
            ky = -1 if y1 > y2 else 1
            for i in range(abs(x1 - x2) + 1):
                c = x1 + i*kx, y1 + i*ky
                add_pipe(c)
            
print(sum([x > 1 for x in pipes.values()]))
            
import printd
printd.imgd(pipes, 
            colourscheme={i:i for i in range(max(pipes.values())+1)},
            scalefactor= 2,
            save=True
            )