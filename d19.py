import numpy as np
import networkx as nx

with open('d19.txt') as f:
    scanners = [line.split('\n')[1:] for line in  f.read().split('\n\n')]
    #reports = {i: [tuple([int(x) for x in c.split(',')]) for c in subline] or i, subline in enumerate([line.split('\n')[1:] for line in  f.read().split('\n\n')])}
    #scanners = [np.array([np.array([int(z) for z in x.split(',')]) for x in y]) for y in scanners]
    
    scanners = {i: [np.array(list(map(int, b.split(',')))) for b in s] for i, s in enumerate(scanners)}
                

# coord_shifts = [np.array([a, b, c]) for a in [-1, 1] for b in [-1, 1] for c in [-1, 1]]
coord_shifts = [np.array([a, b]) for a in [-1, 1] for b in [-1, 1]]

S = nx.DiGraph()
S.add_node(0, shift = np.array([0, 0, 0]))

lS = []

for i, scanner in enumerate(scanners):
    G = nx.DiGraph()
    for a, beacon_a in enumerate(scanners[scanner]):
        for b, beacon_b in enumerate(scanners[scanner]):
            if a != b:
                G.add_edge(a, b, l=beacon_a - beacon_b)
                G.add_edge(b, a, l=beacon_b - beacon_a)
    lS.append(G)

print([[x[y] for y in x] for x in lS])

beakons = scanners[0]

print(beakons)

def shifts(arr):
    r = []
    for a in [arr, np.roll(arr, 1, 1), np. roll(arr, 2, 1)]:
        for cs in coord_shifts:
            r.append(a*cs)
    return r

for sc in scanners:
    for shift in shifts(sc):
        pass