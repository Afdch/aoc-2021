import printd
import random
alphabet = {
    ' ': [  '....',
            '....',
            '....',
            '....',
            '....',
            '....',        
    ],
    'a': [  '.##.',
            '#..#',
            '####',
            '#..#',
            '#..#',
            '#..#',        
    ],
    'b': [  '###.',
            '#..#',
            '###.',
            '#..#',
            '#..#',
            '###.',        
    ],
    'c': [  '.##.',
            '#..#',
            '#...',
            '#...',
            '#..#',
            '.##.',        
    ],
    'd': [  '###.',
            '#..#',
            '#..#',
            '#..#',
            '#..#',
            '#.#.',        
    ],
    'e': [  '####',
            '#...',
            '###.',
            '#...',
            '#...',
            '####',        
    ],
    'f': [  '####',
            '#...',
            '###.',
            '#...',
            '#...',
            '#...',        
    ],
    'g': [  '.##.',
            '#..#',
            '#...',
            '#.##',
            '#..#',
            '.###',        
    ],
    'h': [  '#..#',
            '#..#',
            '####',
            '#..#',
            '#..#',
            '#..#',        
    ],
    'i': [  '.###',
            '..#.',
            '..#.',
            '..#.',
            '..#.',
            '.###',        
    ],
    'j': [  '..##',
            '...#',
            '...#',
            '...#',
            '#..#',
            '.##.',        
    ],
    'k': [  '#..#',
            '#.#.',
            '##..',
            '#.#.',
            '#.#.',
            '#..#',        
    ],
    'l': [  '#...',
            '#...',
            '#...',
            '#...',
            '#...',
            '####',        
    ],
    'm': [  '#..#',
            '####',
            '#..#',
            '#..#',
            '#..#',
            '#..#',        
    ],
    'n': [  '#..#',
            '##.#',
            '##.#',
            '#.##',
            '#.##',
            '#..#',        
    ],
    'o': [  '.##.',
            '#..#',
            '#..#',
            '#..#',
            '#..#',
            '.##.',        
    ],
    'p': [  '###.',
            '#..#',
            '#..#',
            '###.',
            '#...',
            '#...',        
    ],
    'q': [  '.##.',
            '#..#',
            '#..#',
            '#..#',
            '#.##',
            '.###',        
    ],
    'r': [  '###.',
            '#..#',
            '#..#',
            '###.',
            '#..#',
            '#..#',        
    ],
    's': [  '.###',
            '#...',
            '.##.',
            '...#',
            '...#',
            '###.',        
    ],
    't': [  '####',
            '.#..',
            '.#..',
            '.#..',
            '.#..',
            '.#..',        
    ],
    'u': [  '#,,#',
            '#..#',
            '#..#',
            '#..#',
            '#..#',
            '.##.',        
    ],
    'v': [  '#,,#',
            '#..#',
            '#..#',
            '#..#',
            '.##.',
            '.#..',        
    ],
    'w': [  '#,,#',
            '#..#',
            '#..#',
            '#..#',
            '##.#',
            '####',        
    ],
    'x': [  '#..#',
            '#..#',
            '.##.',
            '.##.',
            '#..#',
            '#..#',        
    ],
    'y': [  '#.#.',
            '#.#.',
            '#.#.',
            '.#..',
            '.#..',
            '.#..',        
    ],
    'z': [  '####',
            '...#',
            '..#.',
            '.#..',
            '#...',
            '####',        
    ],
}

def preare_printd(d:dict):
    max_x = max([x for x, y in d])
    max_y = max([y for x, y in d])
    nd = {(i, j): '.' for i in range(max_x+1) for j in range(max_y+1)}
    nd.update({c: '#' for c in d if d[c]})
    printd.printd(nd, {'.':'.', '#':'#', 0:' '}, False)
    
def convert_string(s:str):
    result = ['']*7
    for letter in s:
        for i, substr in enumerate(alphabet[letter.lower()]):
            result[i] += substr + '.'

    return result
        

def unf(d, axis):
    if axis: # X coordinate
        max_x = max([x for x, y in d])+1
        fold_x = random.randint(max_x+1, 2*max_x-1)
        #print(fold_x)
        for p_x, p_y in list(d):
            if p_x >= fold_x - max_x:
                if not bool(random.getrandbits(2)):
                    d[(p_x, p_y)] = False
                    d[(2*max_x - p_x, p_y)] = True
                else:
                    if not bool(random.getrandbits(2)):
                        d[(2*max_x - p_x, p_y)] = True
        return d, max_x
    else:# Y coordinate
        max_y = max([y for x, y in d])+1
        fold_y = random.randint(max_y+1, 2*max_y-1)
        print(fold_y)
        for p_x, p_y in list(d):
            if p_y >= fold_y - max_y:
                if not bool(random.getrandbits(2)):
                    d[(p_x, p_y)] = False
                    d[(p_x, 2*max_y - p_y)] = True
                else:
                    if not bool(random.getrandbits(2)):
                        d[(p_x, 2*max_y - p_y)] = True
                    
        
        return d, max_y
    

def unfold(s, unfolds= 12):
    s = convert_string(s)
    d = {(j, i): True for i, l in enumerate(s) for j,c in enumerate(l) if c=='#'}
    preare_printd(d)
    folds = []
    for _ in range(unfolds):
        axis = bool(random.getrandbits(1))
        d, fold = unf(d, axis)
        folds.append((axis, fold))
    return d, folds

d, folds = unfold("santa loves you")
#print(folds)
#preare_printd(d)


#print(d)

with open('d13-unfold.txt', 'w') as f:
    l = list(d)
    random.shuffle(l)
    for x, y in l:
        if d[(x, y)]:
            f.write(f"{x}, {y}\n")
    f.write("\n")
    for axis, fold in reversed(folds):
        f.write(f"fold along {'x' if axis else 'y'}={fold}\n")
    