BITS_NUMBER = 12

data = [line.strip() for line in open('d3.txt')]
bits = [[item[bit] for item in data] for bit in range(BITS_NUMBER)]

def most_common_is_one(lst):
    ones = 0
    for item in lst:
        if item == '1':
            ones += 1
        else:
            ones -= 1
    return ones >= 0

def get_bits(lst):
    return [[item[bit] for item in lst] for bit in range(BITS_NUMBER)]
    
#part 1
most_common = int(''.join(['1' if most_common_is_one(bits[i]) else '0' for i in range(BITS_NUMBER)]), 2)
least_common = int(''.join(['0' if most_common_is_one(bits[i]) else '1' for i in range(BITS_NUMBER)]) ,2 )

print(most_common * least_common)

from copy import deepcopy
oxy = deepcopy(data)
c02 = deepcopy(data)

for i in range(BITS_NUMBER):
    oxy_b = most_common_is_one(get_bits(oxy)[i])
    co2_b = most_common_is_one(get_bits(c02)[i])
    
    oxy = [x for x in oxy if x[i] == '1' and oxy_b or x[i] == '0' and not oxy_b]
    c02 = [x for x in c02 if x[i] == '1' and not co2_b or x[i] == '0' and co2_b]
    
    if len(oxy) == 1:
        print(oxy_f:=int(''.join(oxy[0]), 2))
    if len(c02) == 1:
        print(co2_f:=int(''.join(c02[0]), 2))
    
print(oxy_f*co2_f)