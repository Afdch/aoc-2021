def part1():
    h_pos, v_pos = 0, 0
    with open('d2.txt') as f:
        for line in f:
            match line.strip().split():
                case 'forward', n:
                    h_pos += int(n)
                case 'up', n:
                    v_pos += int(n)
                case 'down', n:
                    v_pos -= int(n)
    print(- h_pos * v_pos)
    
def part2():
    h_pos, v_pos, aim = 0, 0, 0
    with open('d2.txt') as f:
        for line in f:
            match line.strip().split():
                case 'forward', n:
                    h_pos += int(n)
                    v_pos += aim * int(n)
                case 'up', n:
                    aim += int(n)
                case 'down', n:
                    aim -= int(n)
    print(- h_pos * v_pos)

part1()
part2()
    