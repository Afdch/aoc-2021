import numpy as np
A = np.array([[3, 2, 0], [1, -1, 0], [0, 5, 1]], dtype=int)
B = np.array([[2, 2, 0], [3, -1, 0], [4, 5, 1]], dtype=int)


c = np.array([A, B])

print(A)
C = np.array([1, 2, 3])
print(C)
print(A+C)

coord_shifts = [np.array([a, b, c]) for a in [-1, 1] for b in [-1, 1] for c in [-1, 1]]

for s in coord_shifts:
    print(A*s)