registers = 'wxyz'
vars = {x: 0 for x in registers}

input_buffer = [int(x) for x in '13579246899999']

instructions = []

with open('d24.txt') as f:
    for line in f:
        instructions.append(line.strip().split())

def exec(input_buffer):
    global vars
    for i, instr in enumerate(instructions):
        match instr:
            case 'inp', a:
                vars[a] = input_buffer.pop(0)
                print(vars['z'])
            case 'add', a, b:
                vars[a] += vars[b] if b in registers else int(b)
            case 'mul', a, b:
                vars[a] *= vars[b] if b in registers else int(b)
            case 'div', a, b:
                vars[a] = vars[a] // (vars[b] if b in registers else int(b))
            case 'mod', a, b:
                vars[a] = vars[a] % (vars[b] if b in registers else int(b))
            case 'eql', a, b:
                b = vars[b] if b in registers else int(b)                
                vars[a] = 1 if vars[a] == b else 0

def part1():
    global vars
    start = 99999999999999
    end   = 11111111111110
    for x in range(start, end, -1):
        if '0' in str(x):
            continue
        if x % 9999 == 0:
            print(f'trying {x}', end='\r')
        vars = {x: 0 for x in registers}        
        exec([int(y) for y in str(x)])
        if vars['z'] == 0:
            return x
        
print(part1())