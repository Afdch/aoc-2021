from functools import reduce

def do_id(id, values):
    match id:
        case 0:
            return sum(values)
        case 1:
            return reduce(lambda x, y: x*y, values)
        case 2:
            return min(values)
        case 3:
            return max(values)
        case 5:
            return int(values[0] > values[1])
        case 6:
            return int(values[0] < values[1])
        case 7:
            return int(values[0] == values[1])
        
            
def unpack(bits, outer = False):    
    versions.append(int(bits[0:3], 2))
    type_ID = int(bits[3:6], 2)    
    pack_len = 0
    
    if type_ID == 4: # value
        groups = []
        the_rest = bits[6:]  
        
        while True:
            groups.append(the_rest[1:5])
            cont = bool(int(the_rest[0]))
            the_rest = the_rest[5:]
            if not cont:
                return 6+5*len(groups) + pack_len, int("".join(groups),2)
        
    else: #operator
        op_values = []
        i = 0
        len_ID = bool(int(bits[6]))
        
        if not len_ID: # length is 15-bit number
            length = 22
            to_unpack = bits[22:22+int(bits[7:22], 2)]          
        
        else:   # number of subpackets
            length = 18
            to_unpack = bits[18:]
            subpackets = int(bits[7:18], 2)
        
        while (not len_ID and len(to_unpack) > 0) or (len_ID and i < subpackets):
            r_p, op_value = unpack(to_unpack)
            op_values.append(op_value)
            pack_len += r_p
            to_unpack = to_unpack[r_p:]  
            i += 1
        
        result = do_id(type_ID, op_values)
        
        if outer:
            print(result)
        
        return length + pack_len, result


with open('d16.txt') as f:
    for ln, line in enumerate(f):
        line = line.strip()
        versions = [] 
        bits = bin(int(line, 16))[2:]
        bits = '0'*(len(line)*4-len(bits))+bits
        unpack(bits, True)
        print(sum(versions))