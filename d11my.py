from printd import mkimg, savegif
from random import randint


max_x, max_y = 100, 100
do = {(y, x): randint(0, 9) for y in range(max_y) for x in range(max_x)}

def neigh_diag(c):
    n = []
    y, x = c
    for dy in [-1, 0, 1]:
        for dx in [-1, 0, 1]:
            if 0 <= (ny:=y+dy) < max_y and 0 <= (nx:=x+dx) < max_x:
                n.append((ny, nx))
    return n

history = {}
flashes = 0
step = 0

# visualization
colourscheme = {i:i for i in range(10)}
colourpalette =  [
    236, 212,    43,    #ecd42b
    189, 171,    44,    #bdab2c
    141, 129,    45,    #8d812d
    94,  88,     46,    #5e582e
    82,  78,     46,    #524e2e
    70,  67,     46,    #46432e
    64,  62,     46,    #403e2e
    58,  57,     46,    #3a392e
    52,  52,     46,    #34342e
    46,  46,     46,    #2e2e2e
]

gif = []

#for step in range(100):
while step < 1000:
    # increase all by 1
    for oct in do:
        do[oct] += 1
        
    highlighted = {c for c in do if do[c] > 9}
    flashes += len(highlighted)
    
    # increase neighbours of flashed until stopped
    flashed = set()
    while flashed != highlighted:
        a = set()
        for c in highlighted - flashed:
            for n in neigh_diag(c):
                do[n] += 1
            flashed |= {c}
        a = {c for c in do if do[c] > 9} - flashed
        flashes += len(a)
        highlighted |= a
    
    # reset flashed to 0
    for c in flashed:
        do[c] = 0
    
    
    gif.append(mkimg(do, colourscheme=colourscheme, colourpalette = colourpalette, scalefactor=10))
    
    if all([v == 0 for v in do.values()]):
        print(f"Synchronized first by step {step+1}")
        savegif(gif, 'd11my', loop=False)
        break
    # add to history
    tu = tuple(do.values())
    if tu in history.values():
        print(step, 'was already in this state')
        savegif(gif, f'd11my-{step}', loop=False)
        break
    history[step] = tuple(do.values())
    step += 1
    
    
    if step % 100 == 0:
        print(f"There was {flashes} flashes by step {step}")
        #savegif(gif, f'd11my-{step}', loop=False)
else:
    savegif(gif, f'd11my-{step}', loop=False)   
