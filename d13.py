import printd
dots = {}

folds = []


# with open('d13-unfold.txt') as f:
with open('d13.txt') as f:
    for li, line in enumerate(f):
        line = line.strip().split(',')#.split('=')
        match line:
            case dx, dy:
                # dx, dy = [int(k) for k in line.split(',')]
                dots[(int(dx), int(dy))] = True
            case ['']:
                pass
            case _:
                match (line[0].split('=')):
                    case "fold along x", instruction:
                        folds.append((int(instruction), 0))
                    case "fold along y", instruction:
                        folds.append((0, int(instruction)))



#printd.printd(ndots, tileset, reverse=False)

tileset = {False: ' ', True: '#'}
print(f"Initially there are {sum(dots.values())} dots")
# printd.printd(dots, tileset, reverse=False)

for n, (fold_x, fold_y) in enumerate(folds):
    nndots = {}     
    max_y = max([y for x, y in dots.keys()])
    max_x = max([x for x, y in dots.keys()])
    for dot in dots:
        x, y = dot
        if fold_y > 0:
            if y <= fold_y:
                if (ny:= y + 2*(fold_y - y)) <= max_y:
                    nndots[dot] = dots[dot] or dots[(x, ny)]
                else:
                    nndots[dot] = dots[dot] #####
            else:
                nndots[(x, y - 2*(y - fold_y))] = dots[dot]
        if fold_x > 0:
            if x <= fold_x:
                if (nx:= x + 2*(fold_x - x)) <= max_x:
                    nndots[dot] = dots[dot] or dots[(nx, y)]
                else:
                    nndots[dot] = dots[dot] #####
            else:
                nndots[(x - 2*(x - fold_x), y)] = dots[dot]
                
    dots = nndots
    
    
    print(f"after fold {n+1} there are {len(dots)} dots")
    # printd.printd(dots, tileset, reverse=False)
    
    
max_y = max([y for x, y in dots.keys()])
max_x = max([x for x, y in dots.keys()])

ndots = {(x,y): False for y in range(max_y+1) for x in range(max_x+1)}
ndots.update(dots)
printd.printd(ndots, tileset, reverse=False)