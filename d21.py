pp = [4, 8] # test
pp = [10, 3] # input

pss = [0] * len(pp)

die_rolls = 0

def roll1(player, max_die):
    global die_rolls
    die_rolls += 1
    die_value = die_rolls # part 1
    pp[player] = (pp[player] - 1 + die_value) % max_die + 1

def game1():
    global pp, pss
    while True:
        if any([ps >= 1000 for ps in pss]):
            return
        for player, _ in enumerate(pp):
            for _ in range(3):
                roll1(player, 10)
                if any([ps >= 1000 for ps in pss]):
                    pss[player] += pp[player]                
                    return
            pss[player] += pp[player]
            if any([ps >= 1000 for ps in pss]):
                return

# part1 
game1()
print(min(pss)*die_rolls)


