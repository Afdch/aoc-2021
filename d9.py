numbers = {}
with open('d9.txt') as f:
    for li, line in enumerate(f):
        for ci, c in enumerate(line.strip()):
            numbers[(li, ci)] = int(c)

max_x = max([x for _, x in numbers.keys()])
max_y = max([y for y, _ in numbers.keys()])

def neighbours(c):
    cy, cx = c
    n = []
    if cy > 0:
        n.append((cy-1, cx))
    if cx > 0:
        n.append((cy, cx-1))
    if cy < max_y:
        n.append((cy+1, cx))
    if cx < max_x:
        n.append((cy, cx+1))
    return n

low_points = []
# part 1
for region in numbers:
    if all([numbers[region] < numbers[n] for n in neighbours(region)]):
        low_points.append(region)
print(sum([numbers[x] + 1 for x in low_points]))

processed = set()
basins = {loc: {loc} for loc in low_points}

for low_point in basins:    
    frontier = [low_point]
    while len(frontier )> 0:
        f = frontier.pop()
        for n in neighbours(f):
            if n in processed or numbers[n] == 9 :
                pass
            else:
                frontier.append(n)
                basins[low_point].add(n)
                processed.add(n)
#    print(len(basins[low_point]))

bas_len = sorted([len(basins[x]) for x in basins])[-3:]
print(bas_len[0]*bas_len[1]*bas_len[2])

from printd import imgd, mkimg, savegif

colours = {i: i+1 if i < 9 else 0 for i in range(10)}
imgd(numbers, colourscheme=colours, scalefactor=10, save=True, filename='d9')

gif = []
for i in range(9):
    colours[i] = 1
    gif.append(mkimg(numbers, colourscheme=colours, scalefactor=10))
savegif(gif, 'd9', loop=False)