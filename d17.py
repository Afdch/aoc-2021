st = 'target area: x=102..157, y=-146..-90'
#st = 'target area: x=20..30, y=-10..-5'
st = st.split()[2:]
x = st[0][2:-1].split('..')
y = st[1][2:].split('..')
x_1 = int(x[0])
x_2 = int(x[1])
y_1 = int(y[0])
y_2 = int(y[1])

max_ys = [0]
valid_count = 0
for dx in range(x_2+1):
    for dy in range(y_1, 1000, 1):
        print(f'trying {(dx, dy)}... max y value was {max(max_ys)} in {valid_count} trajectories', end='\r')
        ddx = dx
        ddy = dy
        t = 0
        x, y = 0, 0
        max_y = 0
        valid = False
        while True:
            x += ddx
            y += ddy
            if y_1 <= y <= y_2 and x_1 <= x <= x_2:
                valid = True 
            if y < y_1 or x > x_2:
                if valid:
                    valid_count += 1
                    max_ys.append(max_y)
                break
            max_y = max(y, max_y)
            t += 1
            if ddx > 0:
                ddx -= 1
            elif x < x_1:
                break
            ddy -= 1
print()