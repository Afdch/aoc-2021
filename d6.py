from typing import Counter

ages = {n: 0 for n in range(9)}

with open('d6.txt') as f:
    for line in f:
        f = [int(x) for x in line.strip().split(',')]
ages.update(Counter(f))
print(ages)

for day in range(256):
    new_ages = {n: 0 for n in range(9)}
    for age in range(8):
        new_ages[age] = ages[age + 1]
    new_ages[8] = ages[0]
    new_ages[6] += ages[0]
    ages = new_ages
print(sum(ages.values()))