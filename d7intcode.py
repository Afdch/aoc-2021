# d7intcode
from intcode import intcode

with open('d7.txt') as f:
    for line in f:
        commands = [int(x) for x in line.strip().split(',')]

c = intcode(commands)

print(c.cd())