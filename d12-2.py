import networkx as nx
from networkx.algorithms.simple_paths import all_simple_paths

G = nx.MultiDiGraph()

with open('d12.txt') as f:
    for line in f:
        nodeA, nodeB = line.strip().split('-')
        G.add_edge(nodeA, nodeB)
        G.add_edge(nodeB, nodeA)

big_nodes = [n for n in G.nodes if n.isupper()]
for bn in big_nodes:
    for neighbour in (an:=set(G[bn])):
        for other_neighbour in an-set([neighbour]):
            G.add_edge(neighbour, other_neighbour)
    G.remove_node(bn)

print(len(list(all_simple_paths(G, 'start', 'end'))))