from collections import deque

from typing import Counter


with open('d14.txt') as f:
    lines = f.readlines()
    
template = deque(lines[0].strip())

rules = {a: b for a, b in [l.strip().split(' -> ') for l in lines [2:]]}

for step in range(40):
    insertions = []
    a = template.popleft()
    for i in range(len(template)):
        b = template.popleft()
        template.append(a)
        if (c:=(a+b)) in rules:
            template.append(rules[c])
        a = b
    template.append(a)
        
    #print("".join(template))
    print(step)

C = Counter(template).values()
print(max(C)-min(C))
    