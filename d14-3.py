from collections import Counter


with open('d14.txt') as f:
    lines = f.readlines()
    
template = lines[0].strip()

rules = {a: b for a, b in [l.strip().split(' -> ') for l in lines [2:]]}
counts = {a: 0 for a in rules}

for i in range(len(template)):
    if i > 0:
        if (c:=template[i-1]+template[i]) in rules :
            counts[c] += 1
Cp = {a: 0 for a in rules.values()}
Cp.update(dict(Counter(template)))

# for i in range(10): # part 1
for i in range(40): # part 2
    
    c1 = {a: 0 for a in rules}
    
    for ct in counts:
        a = ct[0]
        b = ct[1]
        c = rules[ct]
        d = counts[ct]
        c1[a+c] += d
        c1[c+b] += d
        Cp[c] += d
    counts = c1

C = Cp.values()
print(max(C)-min(C))