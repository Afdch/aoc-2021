dig_seg = {
    0: 'abcefg',
    1: 'cf',
    2: 'acdeg',
    3: 'acdfg',
    4: 'bcdf',
    5: 'abdfg',
    6: 'abdefg',
    7: 'acf',
    8: 'abcdefg',
    9: 'abcdfg'
}

seg_len = {i: [x for x in dig_seg if len(dig_seg[x]) == i] for i in range(len(dig_seg))}

# print(seg_len)
total_sum = 0
with open('d8.txt') as f:
    inc = 0
    for line in f:
        guessed_numbers = []
        segments = {a: set('abcdefg') for a in 'abcdefg'}
        patterns, values = line.strip().split(' | ')
        patterns, values = patterns.split(), values.split()
        
        # for pattern in patterns:
        #     possible_numbers = seg_len[pattern]
        #     if len(possible_numbers) == 1:
        #         guessed_numbers.append(possible_numbers[0])
        #     for p_n in possible_numbers:
        #         if p_n not in guessed_numbers:
        #print(patterns)
        patterns.sort(key = len)
        #print(patterns)
        
        # number 1 has c and f:
        segments['c'] = set(patterns[0])
        segments['f'] = set(patterns[0])
        # number 7:
        # the top element is an addition to number 1:
        segments['a'] = set(patterns[1]) - set(patterns[0])
        
        # number 4:
        # b, d are unique, c, f as in "1"
        bd = set(patterns[2]) - set(patterns[0])   
        segments['b'] = bd
        segments['d'] = bd
        
        # segments for d and g
        dg = set.intersection(set(patterns[3]), set(patterns[4]), set(patterns[5])) - segments['a']
        segments['d'] = set.intersection(dg, set(patterns[2]))
        segments['g'] = dg - set(patterns[2])
        # and so we also know the b
        segments['b'] -= segments['d']     
        # from 9: it consists of a, b, d ,g which we know, and also cf which we know a pair
        # so e is not in those:
        segments['e'] -= segments['a']
        segments['e'] -= segments['b']
        segments['e'] -= segments['d']
        segments['e'] -= segments['g']
        segments['e'] -= segments['c']  
        
        #finally, we know c is in 0 and in 9 but not in 6:
        fsn = set.intersection(set(patterns[6]),set(patterns[7]),set(patterns[8]))
        fsn -= segments['a']
        fsn -= segments['g']
        fsn -= segments['b']
        segments['c'] -= fsn
        segments['f'] = fsn

        # print(segments)
        
        new_seg = {x: set(list(segments[y])[0] for y in dig_seg[x]) for x in dig_seg}
        
        numbr = []
        for digit in values:
            segs = set(digit)
            #segs = set([list(segments[x])[0] for x in digit])
            for s in new_seg:
                if new_seg[s] == segs:
                    numbr.append(s)
        total_sum += int("".join([str(n) for n in numbr]))
        
    # part 1 
        for pattern in values:
            if len(pattern) in [2, 3, 4, 7]:
                inc += 1
                #print(pattern)
    print(f'Part 1 answer is {inc}')
    print(f'Part 1 answer is {total_sum}')