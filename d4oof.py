with open('d4.txt') as f:
    all_file = ''
    for line in f:
        all_file += line

all_file = [x for x in all_file.split('\n\n')]
numbers = [int(x) for x in all_file[0].split(',')]
boards = all_file[1:]
boards1 = []
boards2 = []
for board in boards:
    board = [[int(x) for x in line.strip().split()]for line in board.split('\n')]
    boards1.append(board)
    board = {board[i][j]: (j, i) for j in range(5) for i in range(5)}
    boards2.append(board)
    
def calc_score(board, n):
    return sum([sum([x for x in board[y] if not isinstance(x, bool)]) for y in range(5)]) * n

boards = [False] * len(boards1)
last_board = None

def board_won(board):
    for j in range(5):
        if [x for x in board[j] if isinstance(x, bool)] == [True] * 5:
            return True        
    
    for i in range(5):
        if [board[o][i] for o in range(5) if isinstance(board[o][i], bool)] == [True] * 5:
            return True
    
def find_winner():
    global last_board
    for n in numbers:
        for en, board in enumerate(boards2):
            if n in board.keys():
                x, y = board[n]
                if not boards[en]:
                    boards1[en][y][x] = True
                    if board_won(boards1[en]):
                        if sum(boards) == len(boards) - 1:
                            index = boards.index(False)
                            print(index)
                            last_board = boards1[index]
                            return(calc_score(last_board, n))
                        boards[en] = True
print(find_winner())