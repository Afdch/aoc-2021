from printd import printd, savegif, imgd, mkimg
sea = {}
max_x, max_y = 0, 0
with open ('d25.txt') as f:
    for li, line in enumerate(f):
        for ci, c in enumerate(line.strip()):
            sea[(ci, li)] = c
            max_x = max(max_x, ci)
        max_y = max(max_y, li)

sea_state = ''.join(sea[(x, y)] for x in range(max_x+1) for y in range(max_y+1))
counter = 0
tiles = {'.': '.', 'v': 'v', '>': '>'}
images = []
printd(sea, None, reverse=False)
while True:
    counter += 1
    for y in range(max_y + 1):
        nl = {}
        for x in range(max_x + 1):
            c = x, y
            if sea[c] == '>':
                if x == max_x:
                    next = 0, y
                else:
                    next = x + 1, y
                if sea[next] == '.':
                    nl[next] = '>'
                    nl[c] = '.'
        sea.update(nl)
    for x in range(max_x + 1):
        nl = {}
        for y in range(max_y + 1):
            c = x, y
            if sea[c] == 'v':
                if y == max_y:
                    next = x, 0
                else:
                    next = x, y + 1
                if sea[next] == '.':
                    nl[next] = 'v'
                    nl[c] = '.'
        sea.update(nl)
    images.append(mkimg(sea, 4, False, {'.': 11, '>': 10, 'v': 9}))
    # print(counter)
    # printd(sea, reverse=False)
    new_state = ''.join(sea[(x, y)] for x in range(max_x+1) for y in range(max_y+1))
    if new_state == sea_state:
        print(counter)
        savegif(images, 'd25')
        break
    else:
        sea_state = new_state
