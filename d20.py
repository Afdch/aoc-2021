from printd import printd
from collections import Counter
idc = {}
with open('d20.txt') as f:
    alg, input_image = f.read().split('\n\n')
    for ln, line in enumerate(input_image.split('\n')):
        for cn, c in enumerate(line):
            idc[(cn, ln)] = c

#printd(idc, {x: x for x in idc.values()}, False)

def get_min_max_dict(d):
    all_x = [x for x, _ in d]
    all_y = [y for _, y in d]
    return min(all_x), max(all_x), min(all_y), max(all_y)


cp = {'.':'0', '#':'1'}
def get_value(c, inf_pixel):
    s = ''
    x, y = c
    for dy in [-1, 0, 1]:
        for dx in [-1, 0, 1]:
            if (dc:=(x+dx, y+dy)) in idc:
                s += cp[idc[dc]]
            else:            
                s += cp[inf_pixel]
    ind = int(s, 2)
    return alg[ind]


def enchance(inf_pixel):
    nidc = {}
    min_x, max_x, min_y, max_y = get_min_max_dict(idc)
    for x in range(min_x - 1, max_x +2, 1):
        for y in range(min_y - 1, max_y + 2, 1):
            nidc[(x, y)] = get_value((x, y), inf_pixel)
    return nidc, inf_pixel

inf_pixel = '.'
ip = {'.': '#', '#':'.'}
# part 1:
for i in range(50):
    print(i+1)
    idc, inf_pixel = enchance(inf_pixel)
    printd(idc, {x: x for x in idc.values()}, False)
    if alg[0] == '#':
        inf_pixel = ip[inf_pixel]
    
print(Counter(idc.values()))