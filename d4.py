with open('d4.txt') as f:
    all_file = ''
    for line in f:
        all_file += line

all_file = [x for x in all_file.split('\n\n')]
numbers = [int(x) for x in all_file[0].split(',')]
boards = [[int(x) for x in line.replace('\n', ' ').split()] for line in all_file[1:]]

def part1():
    global numbers, boards
    for i, number in enumerate(numbers):
        active_nums = numbers[:i+1]        
        for board in boards:
            if  any([all([x in active_nums for x in board[i::5]]) for i in range(5)]) or \
                any([all([x in active_nums for x in board[5*j:j*5 + 5]]) for j in range(5)]):
                print(sum([x for x in board if x not in active_nums]) * number)
                break
        else:
            continue
        break
    
def part2():
    global numbers, boards
    boards_won = set()
    for i, number in enumerate(numbers):
        active_nums = numbers[:i+1]        
        for board_i, board in enumerate(boards):
            if  any([all([x in active_nums for x in board[i::5]]) for i in range(5)]) or \
                any([all([x in active_nums for x in board[5*j:j*5 + 5]]) for j in range(5)]):
                boards_won |= set([board_i])
                if len(boards_won) == len(boards):
                    print(sum([x for x in board if x not in active_nums]) * number)                    
                    break
        else:
            continue
        break
    
    
part2()