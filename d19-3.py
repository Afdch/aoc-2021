import numpy as np
import scipy

with open('d19.txt') as f:
    scanners = [line.split('\n')[1:] for line in  f.read().split('\n\n')]
    scanners = {i: np.array([np.array(list(map(int, b.split(',')))) 
                            for b in s]) for i, s in enumerate(scanners)}

shifts = {i: None for i in scanners}

rotX = np.array([[1, 0, 0],
                [0, 0, -1],
                [0, 1, 0]])
rotZ = np.array([[0, -1, 0],
                 [1, 0, 0],
                 [0, 0, 1]])
rotY = np.array([[0, 0, -1],
                 [0, 1, 0],
                 [1, 0, 0]])
def rot24(Z):
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX @ rotY @ rotY
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX @ rotY
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX @ rotY @ rotY
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX @ rotZ
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX @ rotY @ rotY
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX
    yield Z
    Z = Z @ rotX @ rotY

def find_shift(this, other):
    for rot in rot24(other):
        cdist = scipy.spatial.distance.cdist(this, rot)
        unq, counts = np.unique(cdist, return_counts=True)
        if max(counts) < 12:
            continue
        ind = np.argsort(counts)[-1]        
        z = np.where(cdist == unq[ind])
        offset = this[z[0][0]] - rot[z[1][0]]
        return offset, rot
    return None


shifts[0] = np.array((0, 0, 0))
while any([shifts[x] is None for x in shifts]):
    for s1 in range(len(scanners)):
        if shifts[s1] is None:
            continue
        for s2 in range(len(scanners)):
            if s1 == s2 or shifts[s2] is not None:
                continue
            sh = find_shift(scanners[s1], scanners[s2])
            if sh is None:
                continue
            s_offset, rot = sh
            shifts[s2] = s_offset
            scanners[s2] = rot + s_offset

points = set()
for scanner in scanners:
    points |= set([tuple(x) for x in scanners[scanner]])

print('part 1')
print(len(points))

def manh(d:dict):
    mx = None
    for i in d:
        for j in d:
            if i == j:
                continue
            z = d[i] - d[j]
            s = sum([abs(x) for x in z])            
            mx = max(mx, s) if mx is not None else s
    return mx
            
print(f"part2\n{manh(shifts)}")