import networkx as nx
import matplotlib.pyplot as plt

G = nx.Graph()

with open('d12.txt') as f:
    for line in f:
        nodeA, nodeB = line.strip().split('-')
        G.add_edge(nodeA, nodeB)
        G.add_edge(nodeB, nodeA)

pos = nx.drawing.layout.kamada_kawai_layout(G, weight=None)
nx.draw(G, 
        pos, 
        with_labels= True)
plt.show()
