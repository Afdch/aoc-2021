with open('d19.txt') as f:
    scanners = [line.split('\n')[1:] for line in  f.read().split('\n\n')]
    scanners = {i: {j: tuple(map(int, b.split(','))) for j, b in enumerate(s)} for i, s in enumerate(scanners)}

print(scanners)

def subtr_tuple(a, b):
    return(tuple([a[i] - b[i] for i in range(len(a))]))

edges = {i: set([subtr_tuple(scanners[s][a], scanners[s][b]) for a in scanners[s] for b in scanners[s] if a != b]) for i, s in enumerate(scanners)}

print(edges)