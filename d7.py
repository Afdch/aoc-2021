with open('d7.txt') as f:
    for line in f:
        positions = [int(x) for x in line.strip().split(',')]


delta, delta2 = None, None
for i in range(max(positions) + 1):
    # part 1
    deltas = sum([abs(pos - i) for pos in positions])
    if delta == None or delta > deltas:
        delta = deltas
    # part 2
    deltas = sum([sum(range(abs(pos - i)+1)) for pos in positions])
    if delta2 == None or delta2 > deltas:
        delta2 = deltas

print ( delta, delta2)
