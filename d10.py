oc = {c: i for i, c in enumerate('([{<')}
cc = {c: i for i, c in enumerate(')]}>')}

def rem(s):
    ns =  s.replace('()', '')
    ns = ns.replace('[]', '')
    ns = ns.replace('{}', '')
    ns = ns.replace('<>', '')
    if ns != s:
        ns = rem(ns)
    return ns

sum = 0
incomplete = []
incomplete2 = []
with open('d10.txt') as f:
    for line in f:
        l = rem(line.strip())
        for i, c in enumerate(l):
            if i == len(l) - 1:
                incomplete.append(line)
                incomplete2.append(l)
                break
            if c in oc and (nc:=l[i+1]) in cc and oc[c] != cc[nc]:
                match nc:
                    case ')':
                        sum += 3
                    case ']':
                        sum += 57
                    case '}':
                        sum += 1197
                    case '>':
                        sum += 25137
                break
print(sum)

scores = []
for inc in incomplete2:
    score = 0
    for c in inc[::-1]:
        score *= 5        
        match c:
            case '(':
                score += 1
            case '[':
                score += 2
            case '{':
                score += 3
            case '<':
                score += 4
    scores.append(score)
print(sorted(scores)[len(scores)//2])