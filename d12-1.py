import networkx as nx

G = nx.Graph()

with open('d12.txt') as f:
    for line in f:
        nodeA, nodeB = line.strip().split('-')
        G.add_edge(nodeA, nodeB)
        G.add_edge(nodeB, nodeA)

visited = {x:0 for x in G}
visited['start'] = 2

all_paths = set()

current = [['start']]


while len(current) > 0:
    p = current.pop()
    if p[-1] == 'end':
        all_paths.add(tuple(p))
        continue
    for neigbour in G[p[-1]]:
        if neigbour == 'start':
            continue
        if not neigbour.isupper():
            # part 1
            if neigbour in p:
                continue
                
            # part 2
            match p.count(neigbour):
                case 2:
                    continue
                case 1:
                    if any([y>1 for y in [p.count(x) for x in p if not x.isupper()]]):
                        continue
                
        current.append(p + [neigbour])
print(len(all_paths))