from math import ceil, floor

task = []

def get_depth(l):
    return 1 + max(get_depth(itm) for itm in l) if isinstance(l, list) else 0

def get_max_num(l):
    return [int(x) for x in str(l).replace('[', '').replace(']', '').split(',') if int(x) >= 10]

def extract(s):
    ob = 0
    sf = False
    sp = None
    for i, c in enumerate(s):
        if c == '[':
            ob += 1
        if c == ']':
            ob -= 1
        if ob == 5 and not sf:
            sf = True
            sp = i
        if sf and ob < 5:
            ep = i
            return s[:sp], s[sp:i+1], s[i+1:]

def explode(x):
    x1, r, x2 = extract(str(x))
    a, b = eval(r)
    for i in range(len(x1)-1, 0, -1):
        if x1[i].isdigit():
            j = 0
            while x1[i-j-1].isdigit():
                j+=1   
            x1 = x1[:i-j] + str(int(x1[i-j:i+1]) + a) + x1[i+1:]
            break
    for i in range(len(x2)):
        if x2[i].isdigit():
            j = 0
            while x2[i+j].isdigit():
                j+=1            
            x2 = x2[:i] + str(int(x2[i:i+j]) + b) + x2[i+j:]
            break
    x = x1+'0'+x2
    return x

def split(x):
    pass

def magnitude(l):
    a, b = l
    if isinstance(a, list):
        a = magnitude(a)
    if isinstance(b, list):
        b = magnitude(b)
    return 3*a + 2*b

def add_list(l):
    x = None
    for line in l:
        if x == None:
            x = line
            continue
        else:
            x = [x, line]
            #print('new list:', x)
        
        while True:
            explode_condition = get_depth(x) > 4
            max_x = get_max_num(x)
            
            split_condition = len(max_x) > 0
            
            if explode_condition:
                x = eval(explode(x))
                #print('explode: ', x)
        
            elif split_condition:
                max_x = max_x[0]
                a = floor(max_x/2)
                b = ceil(max_x/2)
                res = str([a, b])
                x1, x2 = str(x).split(str(max_x), 1)
                x = eval(x1 + res + x2)
                #print('split:   ', x)
                
            else:
                #print(x)           
                break
            
    return x


def part1():
    p1 = add_list(task[:])
    print(p1)
    print(magnitude(p1))
    
def part2():
    from itertools import combinations
    
    max_magn = 0
    
    for comb in combinations(task[:], 2):
        max_magn = max(max_magn, magnitude(add_list(list(comb))))
        max_magn = max(max_magn, magnitude(add_list(list(reversed(comb)))))
    
    print (max_magn)


with open('d18.txt') as f:
    for line in f:
        line = eval(line.strip())
        task.append(line)   

part1()

part2()